import cv2
from numpy import *
def check_garage(img_file):
    img = cv2.imread(img_file)
    height, width, channels = img.shape
    mask = zeros((height + 2, width + 2), uint8)
    total_retval = 0
    start_pixels = [
        (700, 100),(2463, 100),
        (700, 500),(2463, 500),
        (700, 1500),(2463, 1500),
        (700, 2000),(2463, 2000),
    ]
    for i in range(len(start_pixels)):
        loDiff = (10,10,10)
        upDiff = (1, 1, 1)
        retval, rect, _, _ = cv2.floodFill(
            img, mask, start_pixels[i], (0, 255, 0), loDiff, upDiff 
        )
        total_retval += retval
    return total_retval > 5100000 # True means garage is closed

