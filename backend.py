#!/usr/bin/env python3

import os
import sys

from flask import Flask
from flask.helpers import send_file
from flask_cors import CORS

import detector

app = Flask(__name__)
CORS(app)

try:
    import RPi.GPIO as GPIO

    channel = 21
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(channel, GPIO.OUT)
except ModuleNotFoundError:
    pass

import time

default_sleep_time = 0.1
FILENAME = "/tmp/latest.jpg"

LAPTOP_CAMERA = "laptop"
PI_CAM = "pi"


@app.route("/photo", methods=["GET"])
def get_latest_photo():
    # take_picture_and_save(FILENAME, mode=get_machine_type())
    return send_file(FILENAME, mimetype="image/jpg")


def get_machine_type():
    machine = sys.argv[1]
    if machine.startswith("pi"):
        return PI_CAM
    return LAPTOP_CAMERA


def toggle_on(pin):
    GPIO.output(pin, GPIO.HIGH)


def toggle_off(pin):
    GPIO.output(pin, GPIO.LOW)


@app.route("/status")
def get_door_status():
    curr_time = int(time.strftime("%H", time.localtime()))
    if curr_time >= 18 or curr_time <= 6:
        return "UNABLE TO DETERMINE"

    is_closed = detector.check_garage(FILENAME)
    return "SHUT" if is_closed else "OPEN"


@app.route("/toggle")
def toggle():
    toggle_on(channel)
    time.sleep(default_sleep_time)
    toggle_off(channel)
    time.sleep(default_sleep_time)
    os.system('/usr/bin/ssh mayuko@192.168.0.150 "mpv doorbell.mp3"')
    return "OK"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
