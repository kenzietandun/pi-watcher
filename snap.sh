#!/usr/bin/env bash

# Snaps a picture regularly

while true
do
/usr/bin/raspistill --timeout 500 --rotation 90 --width 3280 --height 2464 --quality 10 -o /tmp/latest.jpg
done
