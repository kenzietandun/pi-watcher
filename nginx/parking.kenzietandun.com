map $cookie_{{ cookie_key }} $authentication {
	"{{ cookie_value }}" "off";
	default "18+";
}
server {
	root /var/www/parking.kenzietandun.com;
	index index.html index.htm index.nginx-debian.html;
	server_name parking.kenzietandun.com;

	location / {
		try_files $uri $uri/ =404;
		auth_basic_user_file /etc/nginx/htpasswd;
		auth_basic $authentication;
		add_header Set-Cookie "{{ cookie_key }}={{ cookie_value }};max-age=315360000";
	}

	location /photo {
		proxy_pass http://127.0.0.1:5000/photo;
	}

	location /photo/ {
		proxy_pass http://127.0.0.1:5000/photo;
	}

	location /toggle {
		proxy_pass http://127.0.0.1:5000/toggle;
	}

	location /status {
		proxy_pass http://127.0.0.1:5000/status;
	}


	listen 443 ssl; # managed by Certbot
	ssl_certificate /etc/letsencrypt/live/parking.kenzietandun.com/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/parking.kenzietandun.com/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
	if ($host = parking.kenzietandun.com) {
		return 301 https://$host$request_uri;
	} # managed by Certbot


	server_name parking.kenzietandun.com;
	listen 80;
	return 404; # managed by Certbot
}
